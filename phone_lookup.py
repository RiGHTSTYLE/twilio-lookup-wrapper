import random as rnd
import csv
from time import sleep
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException


def getAreaCodes():
  try: 
    with open('us-area-codes.txt', 'r') as f:
      return ["".join(x.split()) for x in f.readlines()]
  except FileNotFoundError:
    sys.exit('Please ensure us-area-codes.txt is in this directory')


def getTwilioAccountInfo():
  try:
    with open('twilio_account_info.txt', 'r') as f:
      account_sid = "".join(f.readline().replace('ACCOUNT_SID=', '').split())
      auth_token = "".join(f.readline().replace('AUTH_TOKEN=', '').split())
      
      return {'account_sid': account_sid, 'auth_token': auth_token}
  except FileNotFoundError:
    sys.exit('Please ensure your twilio_account_info.txt file is in this directory')


def parseData(data, phone_number):
  active_valid_tmobile_network_codes = [26, 160, 490, '26', '160', '490']
  inactive_valid_tmobile_network_codes = [200, 210, 220, 230, 240, 250, 260, 270, 310, 800, '200', '210', '220', '230', '240', '250', '260', '270', '310', '800']

  row = [phone_number, data.carrier.get('name'), data.carrier.get('type'),  data.carrier.get('mobile_country_code'), data.carrier.get('mobile_network_code')]
  
  if data.carrier.get('mobile_network_code') in active_valid_tmobile_network_codes:
    writeCSV('active_tmobile.csv', row)
  if data.carrier.get('mobile_network_code') in inactive_valid_tmobile_network_codes:
    writeCSV('inactive_tmobile.csv', row)

  writeCSV('all.csv', row)


def writeCSV(file_name, row):
  with open(file_name, 'a') as f:
    writer = csv.writer(f)
    writer.writerow(row)

def collectNumbers(client, area_codes):
  active_valid_tmobile_network_codes = [26, 160, 490, '26', '160', '490']
  inactive_valid_tmobile_network_codes = [200, 210, 220, 230, 240, 250, 260, 270, 310, 800, '200', '210', '220', '230', '240', '250', '260', '270', '310', '800']
  while True:
    try:
      phone_number = '+1{}{}'.format(rnd.choice(area_codes), rnd.randint(10**6, 10**7 - 1))
      twilio_data = client.lookups.phone_numbers(phone_number).fetch(type=['carrier'])
      if not twilio_data.carrier.get('error_code'):
        parseData(twilio_data, phone_number)
      sleep(2)
    except TwilioRestException:
      print('Twilio error, continuing after 5 seconds')
      sleep(5)


if __name__ == '__main__':
  area_codes = getAreaCodes()
  twilio_account_info = getTwilioAccountInfo() 
  client = Client(twilio_account_info.get('account_sid'), twilio_account_info.get('auth_token'))
  collectNumbers(client, area_codes)




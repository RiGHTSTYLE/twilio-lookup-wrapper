# About
T-moble has partnerships with Gogo Inc, who is a network provider for certain airlines. The intention is that tmobile users can get in flight texting and wifi for 1 hour ([source](https://support.t-mobile.com/docs/DOC-14943)). The way they determine if you are a tmobile user is by checking your phone number. No other means of authentication are used at the time of this commit. This small script, which uses twilio's api, was created to collect tmobile numbers for the use of Gogo internet's free promotional inflight wifi.

# Files
## phone_lookup.py
This is the actual script

## twilio_account_info.txt
This contains the account sid and the auth token provided by [Twilio](https://www.twilio.com/docs/lookup/api).  
You will need to put your sid and token in appropriate places in this file

## us-area-codes.txt
A simple, newline delimited list of USA area codes used for the random phone number generation

## active_tmobile.csv
The csv that will be filled with the active t-mobile data aggregated from the script

## inactive_tmoblie.csv
The csv that will be filled with the inactive t-mobile data aggregated from the script

## all.csv
The csv that will be filled with all of the data aggregated from the script, regardless of carrier

## twilio directory
Twilio's python module. Source: https://github.com/twilio/twilio-python

# Notes
The twilio api will cost 0.005 USD per api call.  
The script limits to 1 call every 2 seconds as a security measure  
The script limits to 1 call every 5 seconds on an error on the twilio side  

# Special thanks
[Twilio](https://www.twilio.com/) - Thanks for the great api and the great prices  
[Tmobile](https://www.t-mobile.com/) - Thanks for the vector of exploitation  
[Gogo Inc](https://www.gogoair.com) - Your network speeds suck, and so do your prices. Thanks for nothing
